package linkedList;

public class LL {

    private Node head;
    private Node tail;
    private int size;
    public LL() {
        this.size = 0;
    }

    public void firstIndex(int val){ // ll 1,2,3,4,5 --- 5,4,3,2,1
        Node node = new Node(val);
        node.next = head;
        head = node;
        if(tail == null){ // missing these 2 lines
            tail = head;
        }
        size++;

    }

    public void lastIndex(int val){
        if(tail == null){
            firstIndex(val);
            return;
        }
        Node node  = new Node(val);
        tail.next = node;
        tail = node;
        size++;
    }

    public void insert(int val , int index){
        if(index  == 0){
            firstIndex(val);
            return;
        }
        if(index == size){
            lastIndex(val);
            return;
        }

        Node temp = head;
        for(int i = 1; i< index;i++){ // i =0
            temp = temp.next;
        }
        Node node = new Node(val,temp.next);
        temp.next = node;
        size++;
    }

    public void displayLL(){
        Node temp = head;
        while(temp != null){
            System.out.print(temp.value);
            temp = temp.next;
        }
        System.out.println("End");
    }




    private class Node{
        private int value;
        private Node next;

        public Node(int value) {
            this.value = value;
        }
        public Node(int value, Node next) {
            this.value = value;
            this.next = next;
        }
    }
}
