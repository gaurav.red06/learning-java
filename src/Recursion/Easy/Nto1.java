package Recursion.Easy;

public class Nto1 {
    public static void main(String[] args) {
//        func5to1(5);
//        int dig =0;
//        int n = 134268;
//        while(n>0){
//            int r = n%10;
//            dig = dig + r;
//            n = n/10;
////            System.out.println(n);
//        }

        System.out.println(sumofDigits(54321));
    }
    static void funcNto5(int n){
        if(n==0){
            return;
        }
        System.out.println(n);
        funcNto5(n-1);
    }

    static void func5to1(int n){
        if(n==0){
            return;
        }
//        System.out.println(n);
        func5to1(n-1);
        System.out.println(n);
    }

    static int factorial(int n){
        if(n==1){
            return 1;
        }
         return factorial(n-1) * n;
//        System.out.println(n);
    }
    static int sumofDigits(int n){
        if(n ==0){
            return 0;
        }
        return sumofDigits(n / 10) + n % 10;
    }

}
