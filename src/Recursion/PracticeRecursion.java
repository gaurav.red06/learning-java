package Recursion;

public class PracticeRecursion {
    public static void main(String[] args) {
//        int n = 53495;
//        reverseNum(1234);
//        System.out.println(rev);
        String s = "abcd";
        Character ch = 'e';
        System.out.println(s + ch);
    }

    static int fibonacci(int n){
        if(n ==0 || n == 1){
            return n;
        }
        return fibonacci(n-1) + fibonacci(n-2);
    }

    static int factorial(int n){
        if (n == 1){
            return 1;
        }
        return factorial(n-1) * n;
    }

    static int sumOfDigits(int n){
        if (n == 0)
            return 0;
        return n % 10 + sumOfDigits(n/10);
    }

    static int rev = 0;
    static void reverseNum(int n){
        if (n == 0) {
            return ;
        }
        int rem = n%10;
        rev = rev * 10 + rem;
        reverseNum(n/10);
    }
}
