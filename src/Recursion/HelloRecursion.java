package Recursion;

public class HelloRecursion {
    public static void main(String[] args) {
//        helloRecursion("","abc");
        int[] arr = {2,3,5,78,9};
        isPalindrome(arr,2);

    }

    static void helloRecursion(String p, String up){
        if(up.isEmpty() ) {
            System.out.println(p);
            return;
        }
        char ch = up.charAt(0);
        helloRecursion(p+ch,up.substring(1)); // accepting
        helloRecursion(p,up.substring(1)); // rejecting
//        System.out.println(a);
    }

    public static boolean isPalindrome(int[] arr, int start) {
        if (arr.length == 0) { //empty array is a palindrome
            return true;
        }
        //loop from start to the end of array
        for (int i = start; i < arr.length; i++) {
            //loop from end of array to start
            for (int j = arr.length - 1; j >= start; j -= 1) {
                if (arr[i] != arr[j]) {
                    return false;
                }
            }
        }
        return true;
    }
}

