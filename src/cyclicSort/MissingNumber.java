package cyclicSort;

public class MissingNumber {
    public static void main(String[] args) {
        int[] nums = {9,6,4,2,3,5,7,0,1};
        
    }
    public int missingNumber(int[] nums) {
        int index = 0;
        while(index < nums.length){
            int correctIndex = nums[index];
            if(nums[index] != nums[correctIndex]){
                swap(nums,index,correctIndex);
            }else{
                index++;
            }
        }
        for(int i = 0; i < nums.length ;i++){
            if(nums[i] != i){
                return i;
            }
        }
        return nums.length;
    }
    public void swap(int[] nums,int first,int second){
        int temp = nums[first];
        nums[first] = nums[second];
        nums[second] = temp;
    }
}
