package multiThreading;

public class MyThread implements Runnable{

    @Override
    public void run() {
        for(int i = 0; i < 10; i++){
            System.out.println("value of i "+ i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void main(String[] args) {
        MyThread mt = new MyThread();
        Thread t = new Thread(mt);

        MyThread2 m = new MyThread2();
        t.start();
        m.start();

    }
}

