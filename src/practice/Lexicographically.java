package practice;

public class Lexicographically {
    public static void main(String[] args) {
        String s = "abcd";
        doLexicographically(s);
        System.out.println("yes it it Lexicographically " + s);


    }

    static void doLexicographically(String s){
        //make this array palindrome // 0 to n-1 ,
        int n = s.length()-1;
        for(int i =0; i<=n; i++){
            if(!(s.charAt(i) == s.charAt(n-i))){
                s.replace(s.charAt(i),s.charAt(n-i));
            }
        }
        if(isPalindrome(s)){
            return;
        }
    }


    static boolean isPalindrome(String s){
        int start = 0;
        int end = s.length()-1;
        while(start <= end){
            if(s.charAt(start) != s.charAt(end)){
                return false;
            }
            start++;
            end--;
        }
        return true;
    }

}
